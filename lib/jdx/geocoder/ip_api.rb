# frozen_string_literal: true

module JDX
  module Geocoder
    class IpApi
      include Base

      def initialize(term)
        @term = term
      end

      def city
        data['city']
      end

      def country
        data['country']
      end

      def country_code
        data['countryCode']
      end

      def address
        "#{city}, #{state_code} #{postal_code}, #{country}".sub(/^[ ,]*/, '')
      end

      def state
        data['regionName']
      end

      def state_code
        data['region']
      end

      def latitude
        data['lat']
      end

      def longitude
        data['lon']
      end

      def postal_code
        data['zip']
      end

      # coordinate order is longitude, latitude
      def coordinates
        [longitude, latitude]
      end

      private

      def url
        "https://pro.ip-api.com/json/#{escape_term}?key=#{ipapi_key}"
      end

      def serialize_data(response_data)
        response_data
      end

      def handle_response
        response = typhoeus_get
        if response.success?
          data = parse_json(response.body)
          return [true, data] if data['status'] == 'success'

          blacklist_term
        else
          handle_request_error(response)
        end
        false
      end

      def cache_redis
        @cache_redis ||= Geocoder.configuration.ip_cache_db
      end

      def ipapi_key
        @ipapi_key ||= Geocoder.configuration.ipapi_key
      end
    end
  end
end
